export const greetings = (): string => {
    return 'Hello from @sergiodavidposse/myfirstnpmpackage';
};

export const suma = (operandoA: number, operandoB: number): number => {
    return operandoA + operandoB;
};